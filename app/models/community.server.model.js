'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Community Schema
 */
var CommunitySchema = new Schema({
	tittle: {
		type: String,
		required: 'Tittle can\'t be blank',
		trim: true
	},
	subtittle: {
		type: String,
		required: 'Subtittle can\'t be blank'
	},		
	description: {
		type: String,
		required: 'Description can\'t be blank'
	},
	category: {
		type: String,
		required: 'Please select category'
	},	
	status_post: {
		type: String,
		required: 'Please select status'
	},				
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Community', CommunitySchema);