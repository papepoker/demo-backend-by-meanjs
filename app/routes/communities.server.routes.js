'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users');
	var communities = require('../../app/controllers/communities');

	// Communities Routes
	app.route('/communities')
		.get(communities.list)
		.post(users.requiresLogin, communities.create);			

	// Communities by category Routes
	app.route('/communities/list/:categoryId')
		.get(communities.category)
		.post(users.requiresLogin, communities.create);			

	app.route('/communities/:communityId')
		.get(communities.read)
		.put(users.requiresLogin, communities.hasAuthorization, communities.update)
		.delete(users.requiresLogin, communities.hasAuthorization, communities.delete);

	// Finish by binding the Community middleware
	app.param('categoryId', communities.category);
	app.param('communityId', communities.communityByID);
};