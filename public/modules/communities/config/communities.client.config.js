'use strict';

// Configuring the Articles module
angular.module('communities').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Communities', 'communities', 'dropdown', '/communities(/create)?');
		Menus.addSubMenuItem('topbar', 'communities', 'All communities', 'communities');
		Menus.addSubMenuItem('topbar', 'communities', 'Public', 'communities/list/1');
		Menus.addSubMenuItem('topbar', 'communities', 'Corporate', 'communities/list/2');
		Menus.addSubMenuItem('topbar', 'communities', 'University', 'communities/list/3');		
		Menus.addSubMenuItem('topbar', 'communities', 'New Community', 'communities/create');
	}
]);