'use strict';

// Communities controller
angular.module('communities').controller('CommunitiesController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Communities',
	function($scope, $http, $stateParams, $location, Authentication, Communities ) {
		$scope.authentication = Authentication;
		$scope.category = [{id:0,name: 'Please select category'},{id: 1,name: 'Public'},{id: 2, name: 'Corporate'},{id: 3, name: 'University'}];
		$scope.selectedCategory = $scope.category[0];
		$scope.status_post = [{id:0,name: 'Please select status'},{id: 1,name: 'Posted'},{id: 2,name: 'Ban'}];
		$scope.selectedStatus = $scope.status_post[0];
		$scope.categoryID = $stateParams.categoryID;

		 $scope.onFileSelect = function($files) {
    //$files: an array of files selected, each file has name, size, and type.
    	console.log($files);
  }

		//an array of files selected

		// Create new Community
		$scope.create = function() {
			// Create new Community object
			var community = new Communities ({
				image: $scope.onFileSelect,
				tittle: this.tittle,
				subtittle: this.subtittle,
				description: this.description,
				category: $scope.selectedCategory.id,
				status_post: $scope.selectedStatus.id
			});

			// Redirect after save
			community.$save(function(response) {
				$location.path('communities/' + response._id);

				// Clear form fields
				$scope.image = '';
				$scope.tittle = '';
				$scope.subtittle = '';
				$scope.description = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Community
		$scope.remove = function( community ) {
			if ( community ) { community.$remove();

				for (var i in $scope.communities ) {
					if ($scope.communities [i] === community ) {
						$scope.communities.splice(i, 1);
					}
				}
			} else {
				$scope.community.$remove(function() {
					$location.path('communities');
				});
			}
		};

		// Update existing Community
		$scope.update = function() {
			var community = $scope.community ;

			community.$update(function() {
				$location.path('communities/' + community._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Communities by category
		$scope.find = function() {
			$scope.communities = Communities.query();
		};

		// Find a list of Communities by category
		$scope.findCategory = function() {
			$http.get('communities/list/' + $stateParams.categoryID).success (function(data) {
				$scope.communities = data;
			});
		};


		// Find existing Community
		$scope.findOne = function() {
			$scope.community = Communities.get({ 
				communityId: $stateParams.communityId
			});
		};
	}
]);